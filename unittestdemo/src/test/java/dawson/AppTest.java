package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void shouldAnswerWithTrue() {
        App app = new App();
        boolean result = app.shouldAnswerWithTrue();
        assertTrue(result);
    }

        @Test
    public void shouldEchoInput() {
        App app = new App();
        int expected = 5;
        int actual = app.echo(5);
        assertEquals("The echo method should return the same value that is given to it.", app.echo(5), 5);
    }

    @Test
    public void shouldReturnOneMore() {
        App app = new App();
        int expected = 6;
        int actual = app.oneMore(5);
        assertEquals("The oneMore method should return the input value plus one.", expected, actual);
        // The above assertion will fail because the method oneMore is not correctly implemented.
    }
}

